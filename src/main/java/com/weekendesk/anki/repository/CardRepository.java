package com.weekendesk.anki.repository;


import com.weekendesk.anki.entity.CardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository for cards on {@link CardEntity} model object
 *
 * @author fboyer on 02/10/2018.
 */
@Repository
public interface CardRepository extends JpaRepository<CardEntity, Long> {

    /**
     * Get cards for today.
     *
     * @return List<CardEntity> Obtained.
     */
    @Query(
            value = "SELECT * FROM card c " +
                    "WHERE c.response_type_id = NULL " +
                    " 	OR c.response_type_id =3 " +
                    "	OR (c.response_type_id=2 AND c.modified_at <= :nowDay ) " +
                    "	OR (c.response_type_id=1 AND c.modified_at <= :passedDay )",
            nativeQuery = true)
    List<CardEntity> getCardsForToday(@Param("nowDay") LocalDateTime nowDay, @Param("passedDay") LocalDateTime passedDay);
}
