package com.weekendesk.anki.controller;


import com.weekendesk.anki.mapper.DeskMapper;
import com.weekendesk.anki.model.Desk;
import com.weekendesk.anki.model.constants.DeskUriConstants;
import com.weekendesk.anki.service.DeskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Rest controller for Desk.
 *
 * @author fboyer on 02/10/2018
 */
@RestController
@RequestMapping
public class DeskController {

    @Autowired
    private DeskService deskService;

    @Autowired
    private DeskMapper deskMapper;

    /**
     * Rest method for get desk of today.
     *
     * @return Desk obtained.
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = DeskUriConstants.GET, method = RequestMethod.GET)
    public Desk getDeskForToday() {
        return this.deskMapper.entityToModel(this.deskService.getDesk());
    }

    /**
     * Rest method for save desk.
     *
     * @param desk Desk for save.
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = DeskUriConstants.SAVE, method = RequestMethod.POST)
    public void saveDeskResults(@RequestBody final Desk desk) {
        this.deskService.saveDesk(this.deskMapper.modelToEntity(desk));
    }

}
