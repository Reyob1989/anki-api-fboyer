package com.weekendesk.anki.mapper;

/**
 * @author fboyer on 01/10/2018
 */
public interface MappableEntity<M, E> {

    /**
     * Maps {@link E} into {@link M}.
     *
     * @param entity to be mapped.
     * @return Mapped {@link M}.
     */
    M entityToModel(E entity);

    /**
     * Maps {@link M} into {@link E}.
     *
     * @param model to be mapped.
     * @return Mapped {@link E}.
     */
    E modelToEntity(M model);
}