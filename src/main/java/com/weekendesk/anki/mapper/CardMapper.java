package com.weekendesk.anki.mapper;


import com.weekendesk.anki.entity.CardEntity;
import com.weekendesk.anki.model.Card;
import org.mapstruct.Mapper;

/**
 * Mapper for card.
 *
 * @author fboyer on 01/10/2018
 */
@Mapper(componentModel = "spring")
public interface CardMapper extends MappableEntity<Card, CardEntity> {
}
