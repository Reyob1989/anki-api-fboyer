package com.weekendesk.anki.mapper;


import com.weekendesk.anki.entity.DeskEntity;
import com.weekendesk.anki.model.Desk;
import org.mapstruct.Mapper;

/**
 * Mapper for desk.
 *
 * @author fboyer on 01/10/2018
 */
@Mapper(componentModel = "spring", uses = CardMapper.class)
public interface DeskMapper extends MappableList<Desk, DeskEntity> {
}
