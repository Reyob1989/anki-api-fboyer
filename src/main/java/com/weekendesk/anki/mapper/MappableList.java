package com.weekendesk.anki.mapper;

import java.util.List;

/**
 * @author fboyer on 01/10/2018
 */
public interface MappableList<M, E> extends MappableEntity<M, E> {

    /**
     * Maps {@link List <E>} into a {@link List <M>}
     *
     * @param entities to be mapped.
     * @return Mapped {@link List <M>}.
     */
    List<M> entitiesToModels(List<E> entities);

    /**
     * Maps {@link List <M>} into a {@link List <E>}
     *
     * @param models to be mapped.
     * @return Mapped {@link List <M>}.
     */
    List<E> modelsToEntities(List<M> models);

}