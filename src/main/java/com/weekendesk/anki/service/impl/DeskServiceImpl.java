package com.weekendesk.anki.service.impl;


import com.weekendesk.anki.entity.DeskEntity;
import com.weekendesk.anki.service.CardService;
import com.weekendesk.anki.service.DeskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Desk Service implementation.
 *
 * @author fboyer on 01/10/2018
 */
@Service
public class DeskServiceImpl implements DeskService {

    @Autowired
    private CardService cardService;

    @Transactional
    @Override
    public DeskEntity getDesk() {
        final DeskEntity deskEntity = new DeskEntity();
        deskEntity.setCards(this.cardService.getCardsForToday());
        return deskEntity;
    }

    @Transactional
    @Override
    public void saveDesk(final DeskEntity deskEntity) {
        this.cardService.saveCardsResults(deskEntity.getCards());
    }
}
