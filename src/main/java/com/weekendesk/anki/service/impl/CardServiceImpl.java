package com.weekendesk.anki.service.impl;


import com.weekendesk.anki.entity.CardEntity;
import com.weekendesk.anki.repository.CardRepository;
import com.weekendesk.anki.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Card Service implementation.
 *
 * @author fboyer on 01/10/2018
 */
@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private CardRepository cardRepository;

    private static final Integer DAY = 1;
    private static final Integer TWODAY = 2;

    @Transactional
    @Override
    public List<CardEntity> getCardsForToday() {
        final LocalDateTime day = LocalDateTime.now().minusDays(CardServiceImpl.DAY);
        final LocalDateTime days = LocalDateTime.now().minusDays(CardServiceImpl.TWODAY);
        return this.cardRepository.getCardsForToday(day, days);
    }

    @Transactional
    @Override
    public void saveCardsResults(final List<CardEntity> cards) {
        cards.forEach(card -> this.cardRepository.saveAndFlush(card));
    }
}
