package com.weekendesk.anki.service;


import com.weekendesk.anki.entity.CardEntity;

import java.util.List;

/**
 * Card Service interface.
 *
 * @author fboyer on 01/10/2018
 */
public interface CardService {

    /**
     * Get the all cards for today.
     *
     * @return List<CardEntity> obtained.
     */
    List<CardEntity> getCardsForToday();

    /**
     * Save cards results of the day
     *
     * @param cards Cards results to save.
     */
    void saveCardsResults(final List<CardEntity> cards);
}
