package com.weekendesk.anki.service;

import com.weekendesk.anki.entity.DeskEntity;

/**
 * Desk Service interface.
 *
 * @author fboyer on 01/10/2018
 */
public interface DeskService {

    /**
     * Get the GameEntity based on its identifier.
     *
     * @return Obtained GameEntity
     */
    DeskEntity getDesk();

    /**
     * Save the desk based on its identifier.
     */
    void saveDesk(final DeskEntity deskEntity);
}
