package com.weekendesk.anki.converter;

import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Attribute converter to allow JPA to transform the DB value to the corresponding business enumeration {@link ResponseTypeEnum} and vice versa.
 *
 * @author fboyer on 02/10/2018
 */
@Converter
public class ResponseTypeEnumConverter implements AttributeConverter<ResponseTypeEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(final ResponseTypeEnum attribute) {
        return attribute == null ? null : attribute.getValue();
    }

    @Override
    public ResponseTypeEnum convertToEntityAttribute(final Integer dbData) {
        return ResponseTypeEnum.valueOf(dbData);
    }
}
