package com.weekendesk.anki.entity;

import com.weekendesk.anki.converter.ResponseTypeEnumConverter;
import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;

import javax.persistence.*;

/**
 * Card entity for database.
 *
 * @author fboyer on 01/10/2018
 */
@Entity
@Table(name = "card")
public class CardEntity extends AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cardId;
    private String question;
    private String answer;

    @Convert(converter = ResponseTypeEnumConverter.class)
    @Column(name = "responseTypeId")
    private ResponseTypeEnum responseType;

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ResponseTypeEnum getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseTypeEnum responseType) {
        this.responseType = responseType;
    }
}
