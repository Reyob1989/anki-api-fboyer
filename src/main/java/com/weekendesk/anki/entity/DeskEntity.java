package com.weekendesk.anki.entity;

import java.util.List;

/**
 * Desk entity for database.
 *
 * @author fboyer on 01/10/2018
 */
public class DeskEntity extends AuditableEntity {

    private List<CardEntity> cards;

    public List<CardEntity> getCards() {
        return cards;
    }

    public void setCards(List<CardEntity> cards) {
        this.cards = cards;
    }
}
