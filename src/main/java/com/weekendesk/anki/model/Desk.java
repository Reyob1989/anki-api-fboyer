package com.weekendesk.anki.model;

import java.util.List;

public class Desk {

    private List<Card> cards;

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}
