package com.weekendesk.anki.model;

import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;

public class Card {
    private String question;
    private String answer;
    private ResponseTypeEnum responseType;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ResponseTypeEnum getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseTypeEnum responseType) {
        this.responseType = responseType;
    }
}
