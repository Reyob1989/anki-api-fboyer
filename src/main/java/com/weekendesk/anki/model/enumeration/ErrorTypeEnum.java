package com.weekendesk.anki.model.enumeration;

import org.apache.http.HttpStatus;

import java.util.Objects;

/**
 * Error Types used in the REST API responses.
 *
 * @author fboyer on 2017-09-12.
 */
public enum ErrorTypeEnum {

    // @formatter:off
    SERVER_ERROR(1000, "rest.error.serverError", HttpStatus.SC_INTERNAL_SERVER_ERROR),
    MISSING_PARAMETER(1001, "rest.error.missingParameter", HttpStatus.SC_BAD_REQUEST),
    INVALID_PARAMETER(1002, "rest.error.invalidParameter", HttpStatus.SC_BAD_REQUEST),
    INVALID_CREDENTIALS(1003, "rest.error.invalidCredentials", HttpStatus.SC_UNAUTHORIZED),
    ACCESS_DENIED(1004, "rest.error.accessDenied", HttpStatus.SC_FORBIDDEN);

    // @formatter:on
    private final Integer status;
    private final Integer code;
    private final String messageKey;

    ErrorTypeEnum(final Integer code, final String messageKey, final Integer status) {
        this.code = code;
        this.messageKey = messageKey;
        this.status = status;
    }

    /**
     * Resolves a {@link ErrorTypeEnum} from its code.
     *
     * @param code code to look for.
     * @return the found {@link ErrorTypeEnum} or an {value #SERVER_ERROR} instead.
     */
    public static ErrorTypeEnum valueOf(final Integer code) {
        ErrorTypeEnum error = ErrorTypeEnum.SERVER_ERROR;
        for (final ErrorTypeEnum errorTypeEnum : ErrorTypeEnum.values()) {
            if (Objects.equals(errorTypeEnum.code, code)) {
                error = errorTypeEnum;
                break;
            }
        }
        return error;
    }

    /**
     * Returns the error type code.
     *
     * @return Error code.
     */
    public Integer getCode() {
        return this.code;
    }

    /**
     * Returns the error message key to be resolved from a resources file.
     *
     * @return Message key for looking for in resources property files.
     */
    public String getMessageKey() {
        return this.messageKey;
    }

    /**
     * Returns the HttpStatus code for the error.
     *
     * @return HTTP Status Code.
     */
    public Integer getStatus() {
        return this.status;
    }
}
