package com.weekendesk.anki.model.enumeration;

import java.util.Objects;

/**
 * Channel enumeration.
 *
 * @author fboyer on 2017-09-12.
 */
public enum ResponseTypeEnum {
    GREEN(1, "correct"),
    ORANGE(2, "halfcorret"),
    RED(3, "incorrect");

    private static final String ENTITY = "ResponseType";

    private final Integer value;
    private final String literalKey;

    ResponseTypeEnum(final Integer value, final String literalKey) {
        this.value = value;
        this.literalKey = literalKey;
    }

    /**
     * Returns the enum identifier.
     *
     * @return The value.
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * Returns the literal message key to be resolved from a resources file.
     *
     * @return Message key for looking for in resources property files.
     */
    public String getLiteralKey() {
        return this.literalKey;
    }

    /**
     * Resolves a {@link ResponseTypeEnum} from its numeric value.
     *
     * @param value value to look for.
     * @return ChannelEnum.
     * @throws IllegalArgumentException if the given value is not valid.
     */
    public static ResponseTypeEnum valueOf(final Integer value) {
        for (final ResponseTypeEnum instance : ResponseTypeEnum.values()) {
            if (Objects.equals(instance.value, value)) {
                return instance;
            }
        }
        throw new IllegalArgumentException("There isn't any " + ResponseTypeEnum.ENTITY + " with value '" + value + "'.");
    }
}
