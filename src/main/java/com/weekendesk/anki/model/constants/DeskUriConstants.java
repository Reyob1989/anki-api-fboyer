package com.weekendesk.anki.model.constants;

/**
 * Desk REST Service URI Constants
 *
 * @author fboyer on 01/10/2018
 */
@SuppressWarnings("PMD.AvoidConstantsInterface")
public interface DeskUriConstants {

    String DESK = "desk";
    String GET = DeskUriConstants.DESK + "/get";
    String SAVE = DeskUriConstants.DESK + "/save";
}
