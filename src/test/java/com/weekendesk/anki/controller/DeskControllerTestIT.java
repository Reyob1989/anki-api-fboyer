package com.weekendesk.anki.controller;

import com.weekendesk.anki.entity.CardEntity;
import com.weekendesk.anki.model.Card;
import com.weekendesk.anki.model.Desk;
import com.weekendesk.anki.model.constants.DeskUriConstants;
import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;
import com.weekendesk.anki.repository.CardRepository;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Test class for {@link DeskController}
 *
 * @author fboyer on 02/10/2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DeskControllerTestIT {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private CardRepository cardRepository;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + this.port + "/");
        this.cardRepository.deleteAll();
        this.initializeDBMethodForController();
    }

    @Test
    public void getDesk() {
        final ResponseEntity<Desk> response = this.template.getForEntity(this.base.toString() + DeskUriConstants.GET,
                Desk.class);

        assertThat(response.getBody(), is(notNullValue()));
        assertThat(response.getBody().getCards(), is(notNullValue()));
    }

    @Test
    public void saveDesk() {
        final List<Card> cards = new ArrayList<>();
        final Card card = new Card();
        card.setQuestion("question");
        card.setAnswer("answer");
        card.setResponseType(ResponseTypeEnum.RED);
        cards.add(card);
        final Desk desk = new Desk();
        desk.setCards(cards);

        this.template.postForEntity(this.base.toString() + DeskUriConstants.SAVE, desk, Desk.class);

        final List<CardEntity> savedEntities = this.cardRepository.findAll();
        Assert.assertThat(savedEntities.size(), CoreMatchers.is(4));
    }

    public void initializeDBMethodForController() {
        final CardEntity cardEntity = new CardEntity();
        cardEntity.setQuestion("questionA");
        cardEntity.setAnswer("answer");
        cardEntity.setResponseType(ResponseTypeEnum.GREEN);
        this.cardRepository.saveAndFlush(cardEntity);
        final CardEntity cardEntity1 = new CardEntity();
        cardEntity1.setQuestion("questionB");
        cardEntity1.setAnswer("answer1");
        cardEntity1.setResponseType(ResponseTypeEnum.ORANGE);
        this.cardRepository.saveAndFlush(cardEntity1);
        final CardEntity cardEntity2 = new CardEntity();
        cardEntity2.setQuestion("questionC");
        cardEntity2.setAnswer("answer2");
        cardEntity2.setResponseType(ResponseTypeEnum.RED);
        this.cardRepository.saveAndFlush(cardEntity2);
    }
}
