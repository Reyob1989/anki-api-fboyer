package com.weekendesk.anki.repository;

import com.weekendesk.anki.entity.CardEntity;
import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CardRepositoryTestIT {

    @Autowired
    private CardRepository cardRepository;

    private static final Integer DAY = 1;
    private static final Integer TWODAY = 2;

    @Before
    public void cleanDataBase() {
        this.cardRepository.deleteAll();
    }

    @Test
    public void testCrud() {

        final String question = "question";
        final String answer = "answer";
        final ResponseTypeEnum responseType = ResponseTypeEnum.GREEN;

        CardEntity cardEntity = new CardEntity();
        cardEntity.setAnswer(answer);
        cardEntity.setQuestion(question);
        cardEntity.setResponseType(responseType);

        //Create
        final CardEntity cardEntitySaved = this.cardRepository.saveAndFlush(cardEntity);

        Assert.assertEquals(answer, cardEntitySaved.getAnswer());
        Assert.assertEquals(question, cardEntitySaved.getQuestion());
        Assert.assertThat(responseType, CoreMatchers.is(ResponseTypeEnum.GREEN));

        // Get
        cardEntity = this.cardRepository.findById(cardEntity.getCardId()).get();

        Assert.assertEquals(answer, cardEntity.getAnswer());
        Assert.assertEquals(question, cardEntity.getQuestion());
        Assert.assertThat(responseType, CoreMatchers.is(ResponseTypeEnum.GREEN));

        //Update
        final String newQuestion = "questionUpdated";

        cardEntity.setQuestion(newQuestion);

        final CardEntity cardEntityUpdated = this.cardRepository.saveAndFlush(cardEntity);

        Assert.assertEquals(newQuestion, cardEntityUpdated.getQuestion());

        //Delete
        this.cardRepository.delete(cardEntitySaved);
    }

    @Test
    public void GivenCurrentStatusOfDB_WhenCallGetCardsForToday_ThenMustReturnEmptyResults() {
        final LocalDateTime day = LocalDateTime.now().minusDays(CardRepositoryTestIT.DAY);
        final LocalDateTime days = LocalDateTime.now().minusDays(CardRepositoryTestIT.TWODAY);
        final List<CardEntity> listOfCards = this.cardRepository.getCardsForToday(day, days);
        Assert.assertThat(listOfCards.size(), CoreMatchers.is(0));
    }

    @Test
    public void GivenNewCardsDB_WhenCallGetCardsForToday_ThenMustReturnResults() {
        this.initializeDBMethod();
        final LocalDateTime day = LocalDateTime.now().minusDays(CardRepositoryTestIT.DAY);
        final LocalDateTime days = LocalDateTime.now().minusDays(CardRepositoryTestIT.TWODAY);
        final List<CardEntity> listOfCards = this.cardRepository.getCardsForToday(day, days);
        Assert.assertThat(listOfCards.size(), CoreMatchers.is(1));
        Assert.assertThat(listOfCards.get(0).getResponseType(), CoreMatchers.is(ResponseTypeEnum.RED));
    }

    public void initializeDBMethod() {
        final CardEntity cardEntity = new CardEntity();
        cardEntity.setQuestion("questionA");
        cardEntity.setAnswer("answerA");
        cardEntity.setResponseType(ResponseTypeEnum.GREEN);
        this.cardRepository.saveAndFlush(cardEntity);
        final CardEntity cardEntity1 = new CardEntity();
        cardEntity1.setQuestion("questionB");
        cardEntity1.setAnswer("answerB");
        cardEntity1.setResponseType(ResponseTypeEnum.ORANGE);
        this.cardRepository.saveAndFlush(cardEntity1);
        final CardEntity cardEntity2 = new CardEntity();
        cardEntity2.setQuestion("questionC");
        cardEntity2.setAnswer("answerC");
        cardEntity2.setResponseType(ResponseTypeEnum.RED);
        this.cardRepository.saveAndFlush(cardEntity2);
    }
}