package com.weekendesk.anki.service;

import com.weekendesk.anki.entity.CardEntity;
import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;
import com.weekendesk.anki.repository.CardRepository;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CardServiceImplTestIT {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private CardService cardService;

    @Before
    public void initializeDB() {
        this.cardRepository.deleteAll();
        this.initializeDBMethod();
    }

    @Test
    public void GivenValidCards_WhenGetCardsForToday_ThenMustReturnResults() {
        final List<CardEntity> listOfCards = this.cardService.getCardsForToday();
        Assert.assertThat(listOfCards.size(), CoreMatchers.is(1));
        Assert.assertThat(listOfCards.get(0).getResponseType(), CoreMatchers.is(ResponseTypeEnum.RED));
    }

    @Test
    public void GivenValidCards_WhenSaveCardsForToday_ThenMustReturnResults() {
        final List<CardEntity> listOfCards = new ArrayList<>();
        final CardEntity cardEntity = new CardEntity();
        cardEntity.setQuestion("question1");
        cardEntity.setAnswer("answer1");
        cardEntity.setResponseType(ResponseTypeEnum.GREEN);
        listOfCards.add(cardEntity);

        this.cardService.saveCardsResults(listOfCards);

        final List<CardEntity> savedEntities = this.cardRepository.findAll();
        Assert.assertThat(savedEntities.size(), CoreMatchers.is(4));
    }

    public void initializeDBMethod() {
        final CardEntity cardEntity = new CardEntity();
        cardEntity.setQuestion("question1");
        cardEntity.setAnswer("answer1");
        cardEntity.setResponseType(ResponseTypeEnum.GREEN);
        this.cardRepository.saveAndFlush(cardEntity);
        final CardEntity cardEntity1 = new CardEntity();
        cardEntity1.setQuestion("question1");
        cardEntity1.setAnswer("answer1");
        cardEntity1.setResponseType(ResponseTypeEnum.ORANGE);
        this.cardRepository.saveAndFlush(cardEntity1);
        final CardEntity cardEntity2 = new CardEntity();
        cardEntity2.setQuestion("question2");
        cardEntity2.setAnswer("answer2");
        cardEntity2.setResponseType(ResponseTypeEnum.RED);
        this.cardRepository.saveAndFlush(cardEntity2);
    }
}