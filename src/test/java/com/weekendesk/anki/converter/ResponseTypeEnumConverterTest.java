package com.weekendesk.anki.converter;


import com.weekendesk.anki.model.enumeration.ResponseTypeEnum;

/**
 * Test class for {@link ResponseTypeEnumConverter}.
 *
 * @author ecastello on 2017-10-06.
 */
public class ResponseTypeEnumConverterTest extends AbstractConverterTest<ResponseTypeEnumConverter, ResponseTypeEnum, Integer> {

    @Override
    protected Class<ResponseTypeEnumConverter> getClassOf() {
        return ResponseTypeEnumConverter.class;
    }

    @Override
    protected Class<ResponseTypeEnum> getEnumOf() {
        return ResponseTypeEnum.class;
    }
}