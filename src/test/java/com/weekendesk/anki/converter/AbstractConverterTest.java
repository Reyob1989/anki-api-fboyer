package com.weekendesk.anki.converter;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.AttributeConverter;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Abstract class to test converters.
 *
 * @author ecastello on 2017-10-06.
 */
@SuppressWarnings({"JavaReflectionMemberAccess", "unchecked"})
public abstract class AbstractConverterTest<C extends AttributeConverter, E extends Enum<E>, T> {

    private C converter;
    private E enumValue;
    private Method getValueMethod;

    @Before
    public void before() throws Throwable {
        if (this.converter == null) {
            this.converter = this.getClassOf().newInstance();
        }

        if (this.enumValue == null) {
            this.enumValue = this.getEnumOf().getEnumConstants()[0];
        }

        if (this.getValueMethod == null) {
            this.getValueMethod = this.getEnumOf().getDeclaredMethod("getValue");
        }
    }

    @Test
    public void GivenValidEnum_WhenConvertingToDatabase_ThenShouldBeConverted() throws Throwable {
        // Given, When, Then
        assertThat(this.converter.convertToDatabaseColumn(this.enumValue), is(this.getValueMethod.invoke(this.enumValue)));
    }

    @Test
    public void GivenValidDatabaseColum_WhenConvertingToEntity_ThenShouldBeConverted() throws Throwable {
        // Given, When, Then
        assertThat(this.converter.convertToEntityAttribute(this.getValueMethod.invoke(this.enumValue)), is(this.enumValue));
    }

    abstract protected Class<C> getClassOf();

    abstract protected Class<E> getEnumOf();
}